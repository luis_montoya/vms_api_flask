from flask import Flask, jsonify
from flask import request, abort
from flask_cors import CORS

import os

app = Flask(__name__)
CORS(app)

@app.route('/api/permissions', methods=['POST'])
def setPermissions():
	if not request.json or not 'department' in request.json or not 'path' in request.json:
                print(request.json)
                abort(400)

	lst_permissions = []
	failFlag = False
	status = False
	message = None

	if request.json['department'] == 'ICT':
		lst_permissions.append('setfacl -R -m g:sales:rwx,g:quality:rwx,g:ict:rwx,g:ft:rwx,g:superman:rx,g:project:rwx CUSTOMER')
		lst_permissions.append('setfacl -R -m g:ict:rwx,g:quality:rwx,u:produccion:rx,g:superman:rx ICT')
		lst_permissions.append('setfacl -R -m g:design:rwx,g:quality:rwx,g:machined:rx,g:ft:rx,g:ict:rx,g:automation:rx,u:produccion:rx,g:superman:rx MCAD')
		lst_permissions.append('setfacl -R -m g:quality:rwx,g:design:rwx,g:ft:rwx,g:ict:rwx,g:superman:rwx,u:produccion:rx QUALITY')
	elif request.json['department'] == 'FT':
		lst_permissions.append('setfacl -R -m g:sales:rwx,g:quality:rwx,g:ict:rwx,g:ft:rwx,g:superman:rx,g:project:rwx CUSTOMER')
		lst_permissions.append('setfacl -R -m g:ft:rwx,g:quality:rwx,u:produccion:rx,u:produccion:rx FT')
		lst_permissions.append('setfacl -R -m g:design:rwx,g:quality:rwx,g:machined:rx,g:ft:rx,g:ict:rx,g:automation:rx,u:produccion:rx,g:superman:rx MCAD')
		lst_permissions.append('setfacl -R -m g:quality:rwx,g:design:rwx,g:ft:rwx,g:ict:rwx,g:superman:rwx,u:produccion:rx QUALITY')
		lst_permissions.append('setfacl -m g:supply:rwx FT/"MATERIAL QUOTES"')
	elif request.json['department'] == 'AUTOMATION':
		lst_permissions.append('setfacl -R -m g:sales:rwx,g:quality:rwx,g:ict:rwx,g:ft:rwx,g:superman:rx,g:project:rwx CUSTOMER')
		lst_permissions.append('setfacl -R -m g:automation:rwx,g:quality:rwx,u:produccion:rx,g:superman:rx AUTOMATION')
		lst_permissions.append('setfacl -R -m g:design:rwx,g:quality:rwx,g:machined:rx,g:ft:rx,g:ict:rx,g:automation:rx,u:produccion:rx,g:superman:rx MCAD')
		lst_permissions.append('setfacl -R -m g:quality:rwx,g:design:rwx,g:ft:rwx,g:ict:rwx,g:superman:rwx,u:produccion:rx QUALITY')
		lst_permissions.append('setfacl -m g:supply:rwx AUNTOMATION/"MATERIAL QUOTES"')
	elif request.json['department'] == 'LOGISTICS':
		lst_permissions.append('setfacl -R -m g:supply:rwx LOGISTICS')

	lst_permissions.append('setfacl -R -m g:machined:rwx MCAD/"CNC PROGRAMS"')
	lst_permissions.append('setfacl -R -m u:frosas:rwx CUSTOMER/"SAMPLE PICTURES"')


	if os.path.isdir(request.json['path']):
		os.chdir(request.json['path'])
		for permission in lst_permissions:
			print(permission)
			r = os.system(permission)
			if r != 0:
				message = 'ERROR => {0}'.format(permission)
				failFlag = True

		'''os.chdir('MCAD')
		os.system('setfacl -R -m g:machined:rwx "CNC PROGRAMS"')
		os.chdir('../CUSTOMER')
		os.system('setfacl -R -m u:frosas:rwx "SAMPLE PICTURES"')'''

		if not failFlag:
			status = True
	else:
		message = 'PATH DOES NOT EXISTS {path}'.format(path=request.json['path'])



	response = {
		'status': status,
		'message': message
	}

	return jsonify({'response': response}), 200


if __name__ == '__main__':
    app.run(debug=False, host='191.168.0.5', port=11111)
